# Create a merge Request
In order to merge your repository into the original repository, your branch/forked repository have to be merged into the original repository.

## Im finished with editing. What should i do?
Great. Now you have to do following steps:

* [Commit and push everything to your repository](#updating-your-local-repository)
* [Create a merge request](#create-merge-request)
* [Run the Pipeline](#running-the-pipeline)

### Updating your local repository
Im just running quickly over some basics. For more details look into [git-slides](slides_git.pdf).

* With `# git status` you can see the current status of your repostitory (branchname, filechanges etc.). Here you can see if your local repostiory is up to date.
* With `# git add file` you can add a change/file to your repository
* With `# git commit` you commit these adds to the repostiory. Note: Now you have to write a short commit message e.g. "Added XYZ".
* With `# git push` you push your local repository to gitlab.

## Create Merge Request

Once you pushed your branch onto gitlab and it passes the pipeline, you can create a merge request. After pushing it onto gitlab, there should be following message in the terminal:
```
remote: To create a merge request for BrancheName, visit:
remote:   https://gitlab.com/openCFS/userdocu/-/merge_requests/new?merge_request%5Bsource_branch%5D=BrancheName
```

Simply click the link, and create a merge request.

### Running the pipeline
The pipeline is simply a script, which executes certain commandos and looks if no error are occuring. In our case, the pipeline simply builds the website and if errors are occurring the pipeline fails. Otherwise you are good to go. In the next step the compatibility of your merge request regarding the main repository is checked. If your branch/repo can be merged, one admin gets a message and can approve the merge request.


