#!/bin/sh

# mesh (creates domain.cdb)
trelis -batch -nographics -nojournal pipe.jou

# run cfs
cfs -p mech-acou-eigenvalues.xml test
